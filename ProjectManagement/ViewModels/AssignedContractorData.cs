﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectManagement.ViewModels
{
    public class AssignedContractorData
    {
        public int EmployeeId { get; set; }
        public string Title { get; set; }
        public bool Assigned { get; set; }
    }
}