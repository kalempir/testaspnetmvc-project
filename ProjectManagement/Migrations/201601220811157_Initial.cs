namespace ProjectManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Employees",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Firstname = c.String(nullable: false, maxLength: 20),
                        Surname = c.String(nullable: false, maxLength: 20),
                        Lastname = c.String(maxLength: 20),
                        Email = c.String(nullable: false, maxLength: 20),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Projects",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        CustomerCompanyName = c.String(nullable: false, maxLength: 100),
                        ContractorCompanyName = c.String(nullable: false, maxLength: 100),
                        EmployeeId = c.Int(nullable: false),
                        ProjectManagerId = c.Int(nullable: false),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        Priority = c.Int(nullable: false),
                        Description = c.String(maxLength: 250),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Employees", t => t.EmployeeId)
                .ForeignKey("dbo.Employees", t => t.ProjectManagerId)
                .Index(t => t.EmployeeId)
                .Index(t => t.ProjectManagerId);
            
            CreateTable(
                "dbo.ProjectsEmployees",
                c => new
                    {
                        ProjectId = c.Int(nullable: false),
                        EmployeeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ProjectId, t.EmployeeId })
                .ForeignKey("dbo.Projects", t => t.ProjectId, cascadeDelete: true)
                .ForeignKey("dbo.Employees", t => t.EmployeeId, cascadeDelete: true)
                .Index(t => t.ProjectId)
                .Index(t => t.EmployeeId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Projects", "ProjectManagerId", "dbo.Employees");
            DropForeignKey("dbo.Projects", "EmployeeId", "dbo.Employees");
            DropForeignKey("dbo.ProjectsEmployees", "EmployeeId", "dbo.Employees");
            DropForeignKey("dbo.ProjectsEmployees", "ProjectId", "dbo.Projects");
            DropIndex("dbo.ProjectsEmployees", new[] { "EmployeeId" });
            DropIndex("dbo.ProjectsEmployees", new[] { "ProjectId" });
            DropIndex("dbo.Projects", new[] { "ProjectManagerId" });
            DropIndex("dbo.Projects", new[] { "EmployeeId" });
            DropTable("dbo.ProjectsEmployees");
            DropTable("dbo.Projects");
            DropTable("dbo.Employees");
        }
    }
}
