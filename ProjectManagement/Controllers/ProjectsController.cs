﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ProjectManagement.Models;
using ProjectManagement.DBContext;
using ProjectManagement.ViewModels;

namespace ProjectManagement.Controllers
{
    public class ProjectsController : Controller
    {
        private ProjectManagementContext db = new ProjectManagementContext();

        // GET: /Projects/
        public ActionResult Index()
        {
            var projects = db.Projects.Include(p => p.Employee).Include(p => p.ProjectManager);
            return View(projects);
        }

        // GET: /Projects/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Project project = db.Projects.Find(id);
            if (project == null)
            {
                return HttpNotFound();
            }
            return View(project);
        }

        // GET: /Projects/Create
        public ActionResult Create()
        {
            var project = new Project();
            ViewBag.EmployeeId = new SelectList(db.Employees, "Id", "Firstname");
            ViewBag.ProjectManagerId = new SelectList(db.Employees, "Id", "Firstname");
            PopulateAssignedContractorData(project);
            return View();
        }

        // POST: /Projects/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,CustomerCompanyName,ContractorCompanyName,EmployeeId,ProjectManagerId,StartDate,EndDate,Priority,Description")] Project project, string[] selectedContractors)
        {
            if (selectedContractors != null)
            {
                project.ContractorEmployees = new List<Employee>();
                foreach (var contractor in selectedContractors)
                {
                    var employeeToAdd = db.Employees.Find(int.Parse(contractor));
                    project.ContractorEmployees.Add(employeeToAdd);
                }
            }
            if (ModelState.IsValid)
            {
                db.Projects.Add(project);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.EmployeeId = new SelectList(db.Employees, "Id", "Firstname", project.EmployeeId);
            ViewBag.ProjectManagerId = new SelectList(db.Employees, "Id", "Firstname", project.ProjectManagerId);
            PopulateAssignedContractorData(project);
            return View(project);
        }

        // GET: /Projects/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Project project = db.Projects
                .Include(p => p.Employee)
                .Include(p => p.ProjectManager).Single(p => p.Id == id);
            PopulateAssignedContractorData(project);
            if (project == null)
            {
                return HttpNotFound();
            }
            ViewBag.EmployeeId = new SelectList(db.Employees, "Id", "Firstname", project.EmployeeId);
            ViewBag.ProjectManagerId = new SelectList(db.Employees, "Id", "Firstname", project.ProjectManagerId);
            return View(project);
        }

        // POST: /Projects/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int? id, string[] selectedContractors)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Project projectToUpdate = db.Projects
                .Include(p => p.Employee)
                .Include(p => p.ProjectManager).Single(p => p.Id == id);
            if (TryUpdateModel(projectToUpdate, "",
                new string[]
                {
                    "Name", "CustomerCompanyName", "ContractorCompanyName", "EmployeeId", "ProjectManagerId", "StartDate",
                    "EndDate", "Priority", "Description"
                }))
            {
                try
                {
                    UpdateInstructorCourses(selectedContractors, projectToUpdate);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch (RetryLimitExceededException /* dex */)
                {
                    //Log the error (uncomment dex variable name and add a line here to write a log.
                    ModelState.AddModelError("",
                        "Unable to save changes. Try again, and if the problem persists, see your system administrator.");
                }
            }
            ViewBag.EmployeeId = new SelectList(db.Employees, "Id", "Firstname", projectToUpdate.EmployeeId);
            ViewBag.ProjectManagerId = new SelectList(db.Employees, "Id", "Firstname", projectToUpdate.ProjectManagerId);
            PopulateAssignedContractorData(projectToUpdate);
            return View(projectToUpdate);
        }

        private void UpdateInstructorCourses(string[] selectedContractors, Project projectToUpdate)
        {
            if (selectedContractors == null)
            {
                projectToUpdate.ContractorEmployees = new List<Employee>();
                return;
            }

            var selectedContractorsHs = new HashSet<string>(selectedContractors);
            var projectContractors = new HashSet<int>
                (projectToUpdate.ContractorEmployees.Select(ce => ce.Id));
            foreach (var employee in db.Employees)
            {
                if (selectedContractorsHs.Contains(employee.Id.ToString()))
                {
                    if (!projectContractors.Contains(employee.Id))
                    {
                        projectToUpdate.ContractorEmployees.Add(employee);
                    }
                }
                else
                {
                    if (projectContractors.Contains(employee.Id))
                    {
                        projectToUpdate.ContractorEmployees.Remove(employee);
                    }
                }
            }
        }

        // GET: /Projects/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Project project = db.Projects.Find(id);
            if (project == null)
            {
                return HttpNotFound();
            }
            return View(project);
        }

        // POST: /Projects/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Project project = db.Projects.Find(id);
            db.Projects.Remove(project);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private void PopulateAssignedContractorData(Project project)
        {
            var allEmployees = db.Employees;
            var projectContractors = new HashSet<int>(project.ContractorEmployees.Select(ce => ce.Id));
            var viewModel = allEmployees.Select(employee => new AssignedContractorData
            {
                EmployeeId = employee.Id, Title = employee.Firstname, Assigned = projectContractors.Contains(employee.Id)
            }).ToList();
            ViewBag.Contractors = viewModel;
        }
    }
}
