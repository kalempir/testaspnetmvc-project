﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;
using ProjectManagement.Models;

namespace ProjectManagement.DBContext
{
    public class ProjectManagementContext : DbContext
    {
        public ProjectManagementContext() : base()
        {
        }

        public DbSet<Project> Projects { get; set; }
        public DbSet<Employee> Employees { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new ProjectEntityConfiguration());
            modelBuilder.Configurations.Add(new EmployeeEntityConfiguration());
        }
    }

    public class ProjectEntityConfiguration : EntityTypeConfiguration<Project>
    {
        public ProjectEntityConfiguration()
        {
            this.ToTable("Projects");
            this.HasKey(e => e.Id);
            this.Property(p => p.Name).HasMaxLength(100).IsRequired();
            this.Property(p => p.CustomerCompanyName).HasMaxLength(100).IsRequired();
            this.Property(p => p.ContractorCompanyName).HasMaxLength(100).IsRequired();
            this.Property(p => p.Description).HasMaxLength(250);
            this.Property(p => p.StartDate).IsRequired();
            this.Property(p => p.EndDate).IsRequired();
            this.Property(p => p.Priority).IsRequired();

            this.HasRequired<Employee>(p => p.Employee)
                .WithMany(e => e.ProjectsAsEmployee)
                .HasForeignKey(p => p.EmployeeId)
                .WillCascadeOnDelete(false);

            this.HasRequired<Employee>(p => p.ProjectManager)
                .WithMany(e => e.ProjectsAsManager)
                .HasForeignKey(p => p.ProjectManagerId)
                .WillCascadeOnDelete(false);

            this.HasMany<Employee>(p => p.ContractorEmployees)
                .WithMany(e => e.ProjectsAsContractor)
                .Map(cs =>
                {
                    cs.MapLeftKey("ProjectId");
                    cs.MapRightKey("EmployeeId");
                    cs.ToTable("ProjectsEmployees");
                });
        }
    }

    public class EmployeeEntityConfiguration : EntityTypeConfiguration<Employee>
    {
        public EmployeeEntityConfiguration()
        {
            this.ToTable("Employees");
            this.HasKey(e => e.Id);
            this.Property(p => p.Firstname).HasMaxLength(20).IsRequired();
            this.Property(p => p.Surname).HasMaxLength(20).IsRequired();
            this.Property(p => p.Lastname).HasMaxLength(20);
            this.Property(p => p.Email).HasMaxLength(20).IsRequired();
        }
    }
}