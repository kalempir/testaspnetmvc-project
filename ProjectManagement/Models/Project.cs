﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ProjectManagement.Models
{
    public enum Priority
    {
        Низкий = 1,
        Средний = 2,
        Высокий = 3,
        Критический = 4
    }

    public class Project
    {
        public Project()
        {
            this.ContractorEmployees = new HashSet<Employee>();
        }
        public int Id { get; set; }
        [DisplayName("Название")]
        public string Name { get; set; }
        [DisplayName("Компания заказчик")]
        public string CustomerCompanyName { get; set; }
        [DisplayName("Компания исполнитель")]
        public string ContractorCompanyName { get; set; }
        [DisplayName("Сотрудник")]
        public int EmployeeId { get; set; }
        [DisplayName("Руководитель проекта")]
        public int ProjectManagerId { get; set; }
        [DisplayName("Начало")]
        public DateTime StartDate { get; set; }
        [DisplayName("Конец")]
        public DateTime EndDate { get; set; }
        [DisplayName("Приоритет")]
        public int Priority { get; set; }
        [DisplayName("Коментарий")]
        public string Description { get; set; }

        [DisplayName("Сотрудник")]
        public virtual Employee Employee { get; set; }

        [DisplayName("Руководитель проекта")]
        public virtual Employee ProjectManager { get; set; }

        [DisplayName("Исполнители проекта")]
        public virtual ICollection<Employee> ContractorEmployees { get; set; }
    }

}