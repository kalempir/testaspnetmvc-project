﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace ProjectManagement.Models
{
    public class Employee
    {
        public Employee()
        {
            this.ProjectsAsContractor = new HashSet<Project>();
            this.ProjectsAsManager = new HashSet<Project>();
            this.ProjectsAsEmployee = new HashSet<Project>();
        
        }
        public int Id { get; set; }
        [DisplayName("Имя")]
        public string Firstname { get; set; }
        [DisplayName("Фамилия")]
        public string Surname { get; set; }
        [DisplayName("Отчество")]
        public string Lastname { get; set; }
        [DisplayName("Электронная почта")]
        public string Email { get; set; }

        public virtual ICollection<Project> ProjectsAsEmployee { get; set; }
        public virtual ICollection<Project> ProjectsAsManager { get; set; }
        public virtual ICollection<Project> ProjectsAsContractor { get; set; }
    }
}